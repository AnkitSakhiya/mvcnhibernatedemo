SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ProductID], [ProductName], [ProductDescription], [ProductPrice], [IsAvailable]) VALUES (1, N'Product  Name', N'Description', CAST(123.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ProductDescription], [ProductPrice], [IsAvailable]) VALUES (3, N'Product  Name 2', N'Description 2', CAST(1234.00 AS Decimal(18, 2)), 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ProductDescription], [ProductPrice], [IsAvailable]) VALUES (4, N'Laptop', N'Laptop Computer for personal use', CAST(24001.00 AS Decimal(18, 2)), 1)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO