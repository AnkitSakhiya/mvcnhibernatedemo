SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](255) NULL,
	[ProductDescription] [varchar](255) NULL,
	[ProductPrice] [decimal](18, 2) NULL,
	[IsAvailable] [bit] NULL
) ON [PRIMARY]
GO
