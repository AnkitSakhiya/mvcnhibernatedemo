﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCHibernateDemo.Domain.Nh.Interface
{
    public interface IBRepository<T> where T : class
    {
        void Delete(T obj);
        IEnumerable<T> GetAll();
        T GetByID(object id);
        void Insert(T obj);
        void Update(T obj);
    }
}
