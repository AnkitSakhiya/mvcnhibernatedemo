﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCHibernateDemo.Domain.Nh.Model;

namespace MVCHibernateDemo.Domain.Nh.Interface
{
    public interface IProductRepository : IBRepository<Products>
    {
    }
}
