﻿using System.ComponentModel.DataAnnotations;

namespace MVCHibernateDemo.Domain.Nh.Model
{
    public class Products
    {
        [Display(Name = "Product ID")]
        public virtual int ProductID { get; set; }
        [Required]
        [Display(Name = "Product Name")]
        public virtual string ProductName { get; set; }
        [Required]
        [Display(Name = "Product Description")]
        public virtual string ProductDescription { get; set; }
        [Required]
        [Display(Name = "Product Price")]
        public virtual decimal? ProductPrice { get; set; }
        [Display(Name = "Is Available")]
        public virtual bool? IsAvailable { get; set; } = false;

    }
}
