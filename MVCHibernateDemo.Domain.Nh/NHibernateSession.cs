﻿using NHibernate;
using NHibernate.Cfg;
using System;
using System.IO;
using System.Web;

namespace MVCHibernateDemo.Domain.Nh
{
    public class NHibernateSession
    {
        public static ISession OpenSession()
        {
            var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProductConnectionString"].ConnectionString;
            var configuration = new Configuration();
            configuration.DataBaseIntegration(config => { 
                config.Dialect<NHibernate.Dialect.MsSql2012Dialect>();
                config.Driver<NHibernate.Driver.SqlClientDriver>();
                config.ConnectionProvider<NHibernate.Connection.DriverConnectionProvider>();
                config.ConnectionString = connectionString;
            });
            configuration.CurrentSessionContext<NHibernate.Context.WebSessionContext>();
            configuration.AddAssembly(typeof(Model.Products).Assembly);
            ISessionFactory sessionFactory = configuration.BuildSessionFactory();
            return sessionFactory.OpenSession();
        }
    }
}