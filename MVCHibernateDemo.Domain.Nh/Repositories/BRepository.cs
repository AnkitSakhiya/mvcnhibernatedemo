﻿using System;
using System.Collections.Generic;
using NHibernate;
using System.Linq;
using System.Threading.Tasks;
using MVCHibernateDemo.Domain.Nh.Interface;

namespace MVCHibernateDemo.Domain.Nh.Repositories
{
    public class BRepository<T> : IBRepository<T> where T : class
    {
        public readonly ISession _session;
        public BRepository(ISession session)
        {
            _session = session;
        }
        public void Delete(T obj)
        {
            using ITransaction trans = _session.BeginTransaction();
            _session.Delete(obj);
            trans.Commit();
        }

        public IEnumerable<T> GetAll()
        {
            return _session.Query<T>().ToList();
        }

        public T GetByID(object id)
        {
            return _session.Get<T>(id);
        }

        public void Insert(T obj)
        {
            using ITransaction transaction = _session.BeginTransaction();
            _session.Save(obj);
            transaction.Commit();
        }

        public void Update(T obj)
        {
            using ITransaction transaction = _session.BeginTransaction();
            _session.SaveOrUpdate(obj);
            transaction.Commit();
        }
    }
}
