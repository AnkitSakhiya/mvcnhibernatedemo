﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCHibernateDemo.Domain.Nh.Interface;
using MVCHibernateDemo.Domain.Nh.Model;
using NHibernate;

namespace MVCHibernateDemo.Domain.Nh.Repositories
{
    public class ProductRepository : BRepository<Products>, IProductRepository
    {
        public ProductRepository(ISession session) : base(session)
        {
        }
    }
}
