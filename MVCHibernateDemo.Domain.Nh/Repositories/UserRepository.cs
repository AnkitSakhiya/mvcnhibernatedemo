﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCHibernateDemo.Domain.Nh.Interface;
using MVCHibernateDemo.Domain.Nh.Model;
using NHibernate;

namespace MVCHibernateDemo.Domain.Nh.Repositories
{
    public class UserRepository : BRepository<Users>, IUserRepository
    {
        public UserRepository(ISession session) : base(session)
        {
        }

        public Users ValidateLogin(string username, string password)
        {
            return _session.Query<Users>().Where(x => (x.UserName == username || x.Email == username) && x.Password == password).FirstOrDefault();
        }
    }
}
