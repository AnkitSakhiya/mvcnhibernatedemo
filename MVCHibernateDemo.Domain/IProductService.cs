﻿using MVCHibernateDemo.Domain.Nh.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCHibernateDemo.Domain
{
    public interface IProductService
    {
        IEnumerable<Products> GetAll();
        Products GetByID(int id);
        void Insert(Products product);
        void Update(Products product);
        void Delete(Products product);
    }
}
