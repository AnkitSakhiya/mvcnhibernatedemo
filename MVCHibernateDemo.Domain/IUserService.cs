﻿using MVCHibernateDemo.Domain.Nh.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCHibernateDemo.Domain
{
    public interface IUserService
    {
        IEnumerable<Users> GetAll();
        Users GetByID(int id);
        void Insert(Users user);
        void Update(Users user);
        void Delete(Users user);
        Users ValidateLogin(string username, string password);
    }
}
