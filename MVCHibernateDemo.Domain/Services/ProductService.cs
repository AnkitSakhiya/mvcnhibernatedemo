﻿using MVCHibernateDemo.Domain.Nh.Interface;
using MVCHibernateDemo.Domain.Nh.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCHibernateDemo.Domain.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _ProductRepository;
        public ProductService(IProductRepository productRepository)
        {
            this._ProductRepository = productRepository;
        }

        public IEnumerable<Products> GetAll()
        {
            return _ProductRepository.GetAll();
        }

        public Products GetByID(int id)
        {
            return _ProductRepository.GetByID(id);
        }

        public void Insert(Products product)
        {
            _ProductRepository.Insert(product);
        }

        public void Update(Products product)
        {
            _ProductRepository.Update(product);
        }

        public void Delete(Products product)
        {
            _ProductRepository.Delete(product);
        }
    }
}
