﻿using MVCHibernateDemo.Domain.Nh.Interface;
using MVCHibernateDemo.Domain.Nh.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCHibernateDemo.Domain.Services
{
    public class UserService : IUserService
    {

        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public IEnumerable<Users> GetAll()
        {
            return _userRepository.GetAll();
        }

        public Users GetByID(int id)
        {
            return _userRepository.GetByID(id);
        }

        public void Insert(Users product)
        {
            _userRepository.Insert(product);
        }

        public void Update(Users product)
        {
            _userRepository.Update(product);
        }

        public void Delete(Users product)
        {
            _userRepository.Delete(product);
        }

        public Users ValidateLogin(string username, string password)
        {
            return _userRepository.ValidateLogin(username, password);
        }
    }
}
