﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using MVCHibernateDemo.Domain.Nh;
using MVCHibernateDemo.Domain.Nh.Model;
using MVCHibernateDemo.Domain.Nh.Interface;
using MVCHibernateDemo.Domain;

namespace MVCHibernateDemo.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private readonly IProductService _ProductService;
        public ProductsController(IProductService productService)
        {
            this._ProductService = productService;
        }
        // GET: Products
        public ActionResult Index()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        // GET: Products/List
        public ActionResult List()
        {
            IEnumerable<Products> products;

            products = _ProductService.GetAll();

            return Json(products, JsonRequestBehavior.AllowGet);
        }

        // GET: Products/Details/5
        public ActionResult Details(int id)
        {
            Products product = _ProductService.GetByID(id);

            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        [HttpPost]
        public ActionResult Create(Products product)
        {
            try
            {
                _ProductService.Insert(product);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int id)
        {
            Products product = _ProductService.GetByID(id);

            return View(product);
        }

        // POST: Products/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Products product)
        {
            try
            {
                product.ProductID = id;

                _ProductService.Update(product);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int id)
        {
            Products product = _ProductService.GetByID(id);
            ViewBag.SubmitAction = "Confirm delete";
            return View("Edit", product);
        }

        // POST: Products/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Products product = _ProductService.GetByID(id);
                _ProductService.Delete(product);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
