﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using MVCHibernateDemo.Domain.Nh.Repositories;
using MVCHibernateDemo.Domain.Nh.Interface;
using MVCHibernateDemo.Domain.Nh;
using NHibernate;
using MVCHibernateDemo.Domain.Services;
using MVCHibernateDemo.Domain;

namespace MVCHibernateDemo
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static IContainer Container { get; set; }
        protected void Application_Start()
        {
           
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<ProductRepository>().As<IProductRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<ProductService>().As<IProductService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.Register(ctx => { var NhibernateSession = NHibernateSession.OpenSession(); return NhibernateSession; }).As<ISession>();
            Container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(Container));

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
