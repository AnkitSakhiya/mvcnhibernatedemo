﻿using Autofac;
using Autofac.Integration.Mvc;
using MVCHibernateDemo;
using MVCHibernateDemo.Domain;
using MVCHibernateDemo.Domain.Nh;
using MVCHibernateDemo.Domain.Nh.Interface;
using MVCHibernateDemo.Domain.Nh.Repositories;
using MVCHibernateDemo.Domain.Services;
using NHibernate;
using System.Web.Mvc;

namespace MVCNHIbernateDemo.web.test
{
    public class BaseTest
    {
        protected IContainer container = null;
        public BaseTest()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<ProductRepository>().As<IProductRepository>();
            builder.RegisterType<ProductService>().As<IProductService>();
            builder.Register(ctx => { var NhibernateSession = NHibernateSession.OpenSession(); return NhibernateSession; }).As<ISession>();
            container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
