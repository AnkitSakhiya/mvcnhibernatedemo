﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVCHibernateDemo.Controllers;
using MVCHibernateDemo.Domain;
using MVCHibernateDemo.Domain.Nh.Model;
using MVCHibernateDemo.Domain.Services;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Services.Description;

namespace MVCNHIbernateDemo.web.test
{
    [TestClass]
    public class ProductControllerTest : BaseTest
    {
        private readonly IProductService productService;
        private readonly ProductsController productsController;
        public ProductControllerTest()
        {
            productService = container.Resolve<IProductService>();
            productsController = new ProductsController(productService);
        }
        
        [TestMethod]
        public void Create()
        {
            Products products = new Products();
            products.ProductName = "Test Product";
            products.ProductDescription = "Product Created with unit testing";
            products.ProductPrice = 1000;
            products.IsAvailable = true;
            ActionResult viewResult = productsController.Create(products);
            Assert.IsNotNull(viewResult);
        }


        [TestMethod]
        public void List()
        {
            var products = productsController.List();
            Assert.IsNotNull(products);
        }

        [TestMethod]
        public void Update()
        {
            var products = productsController.List();
            var productList = ((JsonResult)products).Data;
            var product = ((IEnumerable<Products>)productList).Last();
            product.ProductPrice = 1100;
            ActionResult viewResult = productsController.Edit(product.ProductID, product);
            Assert.IsNotNull(viewResult);
        }

        [TestMethod]
        public void Detail()
        {
            var products = productsController.List();
            var productList = ((JsonResult)products).Data;
            var product = ((IEnumerable<Products>)productList).Last();
            ActionResult viewResult = productsController.Details(product.ProductID);
            var model = ((ViewResult)viewResult).Model;
            Assert.IsNotNull(model);
        }

        [TestMethod]
        public void Delete()
        {
            var products = productsController.List();
            var productList = ((JsonResult)products).Data;
            var product = ((IEnumerable<Products>)productList).Last();
            ActionResult viewResult = productsController.Delete(product.ProductID);
            Assert.IsNotNull(viewResult);
        }
    }
}
